---
title: License
layout: blog-page
---

<header>
  <h2 class="title"><a href="/license/" class="title-link">License</a></h2>
</header>

hackbytes.com source code is Copyright (c) 2013 by Byron Sanchez, licensed
under the GNU GPL v2.0.

hackbytes.com logo is Copyright (c) 2013 by Byron Sanchez. All rights
reserved.

hackbytes.com copy is Copyright (c) 2013 by Byron Sanchez. All rights
reserved.

hackbytes.com copy "code snippets" in the _posts/ directory and any
subdirectories thereof are Copyright (c) 2013 by Byron Sanchez, licensed
under the MIT license.

hackbytes.com images and any media (defined as binary files that are not
software dependencies) are Copyright (c) 2013 Byron Sanchez, licensed
under Creative Commons: BY-SA Attribution 3.0 unless otherwise noted.
http://creativecommons.org/licenses/by-sa/3.0/

"bg.png" is Copyright (c) by Atle Mo
(subtlepatterns.com), licensed under Creative Commons: Attribution-ShareAlike 3.0.
http://creativecommons.org/licenses/by-sa/3.0/
Modified by Byron Sanchez

"android-icon-16x16.png" is Copyright (c) by Icomoon (icomoon.io) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"bitbucket-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from 
Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"check-icon-16x16.png" is Copyright (c) by Google (google.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"docx-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"docx-icon-24x24.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"github-icon-16x16.png" is Copyright (c) by Icomoon (icomoon.io) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"google-plus-icon-16x16.png" is Copyright (c) by Icomoon (icomoon.io) from 
Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"ios-icon-16x16.png" is Copyright (c) by Icomoon (icomoon.io) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"linkedin-icon-16x16.png" is Copyright (c) by SimpleIcon (simpleicon.com) from 
Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"location-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from 
Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"mail-icon-16x16.png" is Copyright (c) by SimpleIcon (simpleicon.com) from 
Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"odt-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"odt-icon-24x24.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"pdf-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"pdf-icon-24x24.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"person-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"phone-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"sourcecode-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from 
Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"twitter-icon-16x16.png" is Copyright (c) by Elegant Themes (elegantthemes.com) 
from Flaticon (flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"user-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

"website-icon-16x16.png" is Copyright (c) by Freepik (freepik.com) from Flaticon 
(flaticon.com), licensed under Creative Commons: Attribution 3.0.
https://creativecommons.org/licenses/by/3.0/

