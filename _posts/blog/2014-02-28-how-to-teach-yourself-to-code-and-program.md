---
title: "How To Teach Yourself To Code and Program"
author: "Byron Sanchez"
description: false
date: 2014-02-28 23:57:56
category: blog
comments_enabled: true
layout: blog-post
tags: [languages, beginners, lessons]
---

So you've decided you want to be a developer. You want to understand code and be 
a master of computers. You want to tell them what to do instead of struggling to 
figure out what they may or may not be doing. But you don't know where to begin.

I'll help you out.

### Pick a Language...any language

First, pick a language. Any language, ideally one that interests you. But if you 
don't know which language would interest you, just pick anyone.

- Python
- Ruby
- Java
- C#
- PHP
- C
- C++
- Etc. etc. etc.

PHP, Python and Ruby are languages typically used to power dynamic web sites.
Java and C# are powerful languages that are used to power web sites that are
heavy on traffic and are often used in game development and in many other
applications. C is the mother of all languages and is the source for a lot of
linux packages.

There are A LOT of languages. Pick one and learn it's syntax. A lot of what you
learn in your language of choice is transferrable (you won't have to learn your
second or third language "from scratch").

### Learn How To Write And Execute Code In That Language

The best way to learn syntax is by writing programs yourself. This means
defining a problem you want to solve and writing a program that solves it.
However, everyone has to start from scratch, and if you don't know ANY syntax,
you have to start somewhere. Learn the basic core concepts of the language of 
your choice:

- How to write code by following syntax rules
- How to run the written code

Different languages have different syntax rules (how to write code properly so
that the computer can understand it), different data types (eg. number data,
letter data and word data), and different tools to help you create or
manipulate that data. There are lots of free books and resources online that
will teach you. In fact, [here's a whole list of
them](https://github.com/vhf/free-programming-books/blob/master/free-programming-books.md).
Awesome!

There are also lots of great Youtube Channels that have full courses on how to
program. I love
[thenewboston](http://www.youtube.com/user/thenewboston/playlists). This was 
the
channel that helped me get my feet wet with mobile application development when
I began learning how to program. You can also search YouTube for "[language 
name here] tutorial" and look for programming tutorials. Before you know it, 
you'll be writing all kinds of programs!

Different languages are executed differently. PHP is interpretted, while C is
compiled and then the resulting binary can be executed. Java uses the Java 
Virtual Machine (JVM) and C# uses the Common Language Runtime (CLR) to ensure 
compiled code can run on almost any machine. It's okay if you don't know what 
any of this means for now. But learn how the language you are choosing is 
executed- how the computer knows to run the code.

### Start Writing Programs

Once you have a basic grasp of the syntax as well as how the language is 
executed, start writing programs. Here is a key lesson that may not be obvious.

> It is okay to research when you write your programs.

It is okay to google. Developers google a LOT. Googling saves time. Googling 
cuts cost, effort and increases efficiency. And there are lots of things you 
will not know on your own without googling.

You will almost never write non-trivial programs off the top of your head without 
consulting external resources.

There are many situations in software development where you *will not* be able 
to derive a solution on your own. You *will* need to use an external resource to 
solve a problem. For example, you would not be able to intuitively deduce [how to
make an app work on
Android](http://developer.android.com/reference/android/app/Activity.html), even if you perfectly knew the syntax of the
language Android apps use (Java).  

That previous link is to a page of the Android developer documentation. The
documentation contains a complete listing of methods and functionality you can
use when you write apps for Android. This functionality is collectively known as
an Application Programming Interface (API).

There is no way you can know what the API allows, what functions or methods you
can use, without doing research. This is a common theme in programming. You need
to research. Get good at researching. Get good at understanding your research.
And based on what you learn, you can come up with a solution for the problem you
are trying to solve.

While we're at it, here is another key lesson.

> Documentation is a developer's best friend.

Developers are constantly writing code and consulting external resources,
especially API documentation.

If you check out the Android API, you'll see it's HUGE. How is a developer 
supposed to learn all of that?

Languages and APIs typically have A LOT of tools and functionality. You will 
not be able to learn them all without devoting a ton of time, and this will be 
a wasted effort. APIs frequently change. Functions and methods get deprecated, 
new ones get introduced, bugs are found and fixed, exploits are discovered and 
patched, software [rots](http://en.wikipedia.org/wiki/Software_rot). And thus, 
APIs are constantly changing, evolving and even growing. This is the nature of 
the beast.

Here's how to make sure you don't waste your time.

> The trick to efficient programming is to learn what you need when you need 
> it.

You don't have to know every single function or method a language library 
offers. Just focus on learning and understanding what you need in order to 
solve your problem. As you write more programs and solve more problems, you'll 
begin to see what is commonly used, what is not, and how to learn to do what 
you need to do should you ever be confronted with a new problem you've never 
solved before.

### Conclusion

So to conclude, a quick recap. In order to start learning how to develop, you 
must:

1. Pick a language
2. Learn how to write and execute code in that language
3. Start writing programs!

And it's okay to use external resources! In fact, you should get good at
researching and understanding what you find.

That's it for now!

