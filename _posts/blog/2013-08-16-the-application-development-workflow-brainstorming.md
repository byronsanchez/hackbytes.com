---
title: "The Application Development Workflow: Brainstorming"
description: false
date: 2013-08-16 14:03:12
category: blog
comments_enabled: true
layout: blog-post
tags: [android, ios, mobile, development, brainstorming, market research]
---

### Introduction

Everything begins with an idea.

However, the dirty little secret of most (if not all) ideas is that
ideas are worthless on their own. Ideas are absolutely not valuable on
their own. They are a **starting point** and nothing more. I say this
because you might end up finding the same thing I did- you may come up
with several ideas that you think are novel that have not yet been
implemented. Yet no matter how new the idea may seem, during market
research, you will see that *someone* has tried to create your idea
before in some way. If you are not prepared for this, it can be
seriously demotivating. Ideas are the starting point. But there is a lot
more work that comes after. This document focuses on how to develop
ideas in preparation for all that work that will come after.

### List your ideas

Have a LOT of ideas. Brainstorm until you are blue in the face. Have
good ideas, have bad ideas, get it all out of your system. Build a long
list of ideas for applications to develop. When you are comfortable with
your list, you can begin sorting through each idea.

Place each idea in one of three groups - Not Developing, Maybe
Developing, and Developing.

  - **Not Developing** - This group will contain all ideas you will not
    pursue developing and that you do not see yourself developing
    anytime in the future.

  - **Maybe Developing** - This group will contain all ideas that you
    may want to pursue developing in the future.

  - **Developing** - This group will contain all ideas that you may want
    to pursue developing immediately.

How you sort through your list depends on your motivations for
developing an application. Are you looking to make money? Sort based on
what may bring in the most revenue. Are you developing for fun? Sort
based on what most interests you. Are you looking to build a portfolio?
Sort based on what would be most persuasive to potential employers
looking for new hires in the field you are pursuing.

Ultimately, your goal should be to have five ideas in the "Developing"
Group. The rest should go in either the "Not Developing" group or the
"Maybe Developing" group.

### Define Your Goals

Once you have created a solid list of ideas, you must define the goal
for each idea listed under the "Developing" category. Each idea can have
its own separate goal, independent of the others. Defining these goals
will help you further narrow down which ideas you are willing to
commit to.

The goal can be whatever you want. Some examples include the following-
to learn how to develop for the platforms of your choice, to build a
portfolio, to try and build a financial revenue stream, to expand a
business etc.

**Note:** One word of caution- financial revenue is NEVER a guaranteed
outcome. Like any other business, a for-profit venture does contain its
risks. In addition, you must be aware of your tax laws in case you do
end up building a product that brings in revenue- in this case, you will
have literally created a business.

### Perform Market Research

***Why perform market research***

The goal of market research is twofold - you must understand the
competitive environment of the market you are entering and you must
learn what potential users are expecting of your application. This is
important for any project, including open-source software. The
lifeblood of any application, and of all software in general, is its
users. If you are not competing for price and value in the commercial
sense, then you will almost certainly be competing for a users time. In
either case, it becomes clear that you will have competition in some
way. Do not neglect proper research simply because you may not have
financial motives.

Market research unlocks a treasure-trove of information that will be
very helpful in developing an application that matches and even exceeds
user expectations as well as information regarding the market ecosystem
and how your application may fit in it, depending on the developmental
choices you will make. This information includes things like- expected
features and functionality, how to reach more users, how to retain
users, how to stay relevant, and what users are willing to pay for.
With this knowledge, you can build an application that users will enjoy,
and find ways to differentiate your product.

Your market research process may evolve as you discover the most
effective means of research for your particular market. But a
boilerplate sample plan can certainly help if you have no previous
experience with this kind of research.

***How to perform market research***

Market research is one of the components in the application development
process where you will be straying into another field. Large companies
have teams dedicated to marketing, market research, etc. Here, you will
do your best as you go along, constantly learning. You may not have a
large team, but there is a lot of useful data that you can find and
analyze on your own.

For each of the applications in your "Developing" list, gather the
following information using whatever resources are available to you
(search engines, Google Play App Store, iTunes App Store, etc.; also, be
sure to save your research in a txt or doc file for future reference):

1. **Identify your target market and your target users** - parents,
teens, mid-twenty-year-olds, gamers, musicians, book-lovers, calligraphy
enthusiasts, etc. The more specifically and clearly you define your
target market, the more effective your actual marketing will be. You
don't have to limit yourself to one market category per application.
However, "targeting everyone" is not going to be very helpful.
  - Speak with people who may fall under your target market. People who
    are interested in similar applications. Ask them what they like and
    what they dislike about the similar applications. If you can get
    this info directly from potential users, you will be off to a great
    start. This kind of information is valuable.
  - Your target market are those who you must persuade to download the
    application. Your target users are those who will actually *use* the
    application. Usually, both are the same, however there are cases
    where they are different. If they are different for your case, be
    sure to identify the category under which your target market falls
    and the category under which your target users fall. One very common
    example are "children's apps" whose target market are parents and
    their target users are the children.

2. **Identify your competition** - Identify at least 5 applications that
are similar to your idea that currently dominate the marketplace. Take
notice of the general number of similar applications that exist and
write that number down. Then, gather the following data for each of the
applications you have chosen to analyze.
  - Application URLs - The app store URL for the product and their
    website url, if they have one.
  - Application Description
  - Application Category - The store category that the application is
    listed under.
  - Application Maintenance - The current release version as well as the
    date of the last update.
  - Application Size - The amount of storage space the application
    requires.
  - Target Market - Who is being persuaded to download the application?
  - Target Users - Who is actually *using* the application. Often, this
    is the same as the Target Market, but not always.
  - Hook - What is the applications main "selling point"? Is there
    something about it that makes potential users curious and persuades
    them to check it out and click download? It *can* be gimmicky and it
    does not have to be very useful. However, it will convince potential
    users to download the application. Not all applications have one and
    not all application developers know they have added one.
  - Pros - The good stuff the application offers. Download the
    application, use it and make your own observations. Then, read the
    user ratings and take note of what others liked about it.
  - Cons - The bad stuff that hurts the overall application. Again,
    download the application and make your own observations. Then,
    refer to the customer ratings and take note of what others did not
    like about it.
  - Screenshots - How many screenshots are being displayed? Are they
    tablet screenshots? Are they mobile screenshots? What parts of the
    application are being displayed?
  - Videos - Take notes on how they use video to market their product.
  - Store Listing Design - Observe the app store page design,
    specifically how the product is being marketed in the context of the
    overall page. Look for tailored styles, custom images, anything that
    is not a part of the default app store. Take note of anything you
    find. You may have to decide whether or not you wish to spend time
    designing your own assets (e.g., a banner "feature graphic" for the
    Google Play store). You also want to take note on how they use
    these styles, images and other assets to market their product.

3. **Identify your product's hook** - You should have at least one major
differentiator for your own application. You have knowledge on what is
already out there in the market. Now how will your product be different?
It doesn't have to be anything special or revolutionary, but it can be.
It could also be some gimmicky feature that would capture interest and
persuade potential users to download the application. The overarching
point here is that you will almost always have competition, and lots of
it. Giving yourself a hook, a differentiator, is a good attempt at
getting some share of the market for your product. It is certainly not a
guarantee, but depending on the hook, it can increase your chances in
successfully building a user-base.

4. **(Optional) Identify potential revenue channels** - For each of the
ideas in your "Developing" list, determine whether or not you wish for
it to be a for-profit commercial product. If so, identify potential
revenue channels.
  - **Advertisements** - This can bring in some revenue if you end up
    having a LOT of users. However, ads are not appropriate for all
    applications. For example, you may decide that placing ads in
    applications that target toddlers (by marketing to parents as a
    "children's application") is not a very good idea. Decide whether or
    not your application is a suitable candidate for ads and whether or
    not you want to implement them.
  - **In-App Purchases** - These days, In-App purchases are definitely
    something to consider if you are seeking to bring in revenue. Avoid
    compromising the experience of the application in anyway that makes
    it unfair to the free users. For example, a game where the paying
    players always win may immediately lose value; the free players (who
    are potential customers) may simply stop using the application, and
    consequently, you may end up with a smaller and smaller pool of
    paying customers.
  - **Premium Version** - You may consider making a paid premium version
    of your application. This version would offer more features that
    enhance the application experience while still allowing non-paying
    users to have access to the free version. I would consider making
    this an In-App purchase as opposed to a separate application. That
    way, the upgrade more accessible and will allow for users to upgrade
    at their leisure.
  - **Other** - You can get creative and find other ways to monetize
    your application. The previous suggestions tend to be the standard
    methods of bringing in revenue. But if you know of another method or
    if you can create your own, *and* if it follows the terms and
    conditions of the platforms you plan to support, then go for it
    and give it a shot!

5. **(Optional) Identify localizations you wish to support** - The more
languages your application supports, the wider the net you will be able
to cast for building a user-base. All mobile application platforms offer
localization support for text and images. So plan ahead of time whether
or not you intend to support other languages. If you speak other
languages, consider providing support for those languages. Be sure to
take into consideration whether or not a market exists in the languages
for which your are considering providing support.

### Make Your Decision

After having completely racked your brains for application ideas,
filtering down that list, and thoroughly researching the market, you now
have enough information to decide whether or not you really wish to
pursue any of the projects in your "Developing" list. Eliminate those
that no longer interest you. Then simply decide here and now whether or
not you are truly willing to commit to completing the ones that remain.

I would, at maximum, work on two or three personal projects at a time.
The more projects you take on at one time, the longer they will take for
you to complete.

If you have decided you do not want to pursue any of the ideas on your
"Developing" list, you can do one of two things. You can refer to your
"Maybe Developing" list and reconsider some of those ideas. Or you can
start again from the beginning and come up with a whole new list of
ideas.

### Conclusion

The entire Brainstorming Phase may take days or it may take a few weeks.
Do not get discouraged if you find yourself spending a week or more on
it. Remember, you will probably spend months working on the project
itself. You do not want to end up starting work on something on a whim,
only to find out later that you are no longer interested in the project,
or that it may not be profitable, or any other line of reasoning that
could have been prevented with proper planning and research.

There is a lot of information here. Use it as a baseline and tailor it
to your particular goals. The ultimate point is that you want to have
well-researched data and make well-reasoned assessments that will help
you understand the scope of the project you are taking on and its
potential requirements for completion. Hold on to all the information
you have gathered so far. It is going to be organized in the next phase
as part of your project documentation.

