---
title: "Software Development Methodology Cheat Sheet"
author: "Byron Sanchez"
description: false
date: 2014-03-09 23:40:07
category: blog
comments_enabled: true
layout: blog-post
tags: [methodlogies, cheat sheet, agile, waterfall, scrum, xp, lean, cowboy coding, iterative, incremental, no silver bullet]
---

Here is a quick software development methodology cheat sheet.

**Note:** There are many specific processes defined for some of the
methodologies that are beyond the scope of this article. The main goal of this
article is to give very high-level definitions of each methodology.

  - **Waterfall** - You do ALL of one phase, then ALL of the next, then ALL the
    next (eg. design -> coding -> testing).

  - **Agile** - Agile is a philosophy, not a step-by-step methodology[^1]. It is
    a set of attitudes about software development with the goal of improving
    communication and the overall development process for all parties involved.
    You are "agile" if you align yourself with [the agile
    philosophy](http://agilemanifesto.org/) and make your actions in development
    reflective of that.

  - **Scrum** - A template of steps to follow for the development cycle. It is
    an implementation of agile- it is supposed to follow the principles and
    attitudes of the agile philosophy. At the same time, it actually gives you
    the steps that you can follow. In this methodology, you have a prioritized
    todo list, and you incrementally and iteratively complete chunks of the todo
    list. By performing incremental and iterative development, you allow
    requirements to be flexible and changeable since you are not commiting to
    completing one entire phase before moving to another one (waterfall).
    Thus, the development process can better cope with inevitable changes.

  - **Extreme Programming (XP)** - XP is a methodology which implements [a set of
    rules](http://www.extremeprogramming.org/rules.html) that you follow to
    optimize software development and make it easier to collaborate and work
    with a team while sustaining a high level of software quality. XP is an
    agile methodology as it aligns with agile principles.

  - **Kanban** - A software development process that aims to manage the flow of
    items being worked on (for example, the flow of completion of each item on
    the project's todo list). Kanban processes seek to identify and eliminate
    bottlenecks in the software development lifecycle.

  - **Lean** - Product development principles that have been translated to the
    software development domain from [lean
    manufacturing](http://en.wikipedia.org/wiki/Lean_manufacturing). There are 7
    principles- eliminate waste, build in quality, create knowledge, defer
    commitment, deliver quickly, respect people, and optimize the whole.

  - **Cowboy Coding** - Developers have complete control of software
    development. There is no formal structure that guides the project along. The
    easiest way to think of this is as a "free for all" where you can do
    whatever part of the project you want whenever you want.

  - **Iterative Development** - The software is built knowing that future
    versions (iterations) will be used to refine the project.

  - **Incremental Development** - A software project is divided into chunks and
    each chunk is developed by the team, one at a time.

  - **No Silver Bullet** - A paper written in 1986 by Fred Brooks, in which he
    argues that *"there is no single development, in either technology or
    management technique, which by itself promises even one order of magnitude
    improvement in productivity, in reliability, in simplicity."*

Let me know if I've missed anything and I'll put it up!

[^1]: In the real world, a lot of people mix up agile and scrum. Sometimes they
  mix up agile and some other completely unrelated methodology. Just remember that
  agile is the philosophy, scrum is a methodology, and people will probably get
  them mixed up.

