desc "Build the application and run all tests"
task :test do
  # TODO: build and invoke tests
end

desc "Builds tests without running them"
task :'build-tests' do
  # TODO: build tests
end

desc "Runs tests without building them"
task :'run-tests' do
  # TODO: invoke tests
end

